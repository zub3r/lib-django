from django.db import models
from django.utils.translation import ugettext_lazy as _


class Tag(models.Model):
    slug = models.SlugField(_(u"Tag Code"), max_length=20)
    picture = models.ImageField(_(u"Image"), upload_to="images/tag_pictures/")

    def save(self, *args, **kwargs):
        self.slug = self.slug.lower()
        super(Tag, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.slug


class Doc(models.Model):
    tags = models.ManyToManyField(Tag)
    title = models.CharField(_(u"Document Title"), max_length=511)
    version_date = models.DateField()
    last_update = models.DateTimeField()

    def tags_list(self):
        return [self.tags.all().values('slug')[i]['slug'] for i in range(self.tags.all().count())]

    def save(self, *args, **kwargs):
        if self.pk is None:
            self.title = self.title.title()
        super(Doc, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.title


class Chapter(models.Model):
    class Meta:
        unique_together = ('doc', 'order')

    doc = models.ForeignKey(Doc, related_name="chapters")
    order = models.PositiveSmallIntegerField(_(u"Order"))
    weight = models.PositiveSmallIntegerField(_(u"Weight"))
    title = models.CharField(max_length=511)
    text = models.TextField(_(u"Text"), blank=True)

    def save(self, *args, **kwargs):
        if self.pk is None:
            self.title = self.title.title()
        super(Chapter, self).save(*args, **kwargs)

    def __unicode__(self):
        return '%s, %d' % (self.title, self.order)
