from django.http import JsonResponse
from .models import Tag, Doc


def tags_list(request):
    data = [{"slug": x.slug, "picture": str(x.picture)} for x in Tag.objects.all()]
    return JsonResponse(data, safe=False)


def docs_list(request, tag_slug):
    docs = Doc.objects.filter(tags__slug=tag_slug)
    data = [{"id": x.id,
             "tags": x.tags_list(),
             "title": x.title,
             "version_date": x.version_date,
             "last_update": x.last_update} for x in docs]
    return JsonResponse(data, safe=False)


def doc_detail(request, doc_pk):
    doc = Doc.objects.get(id=doc_pk)
    data = dict()
    if 'chapters' not in request.GET.keys():
        data['chapters'] = (dict((x.id, {"order": x.order,
                                         "weight": x.weight,
                                         "title": x.title,
                                         "text": x.text}) for x in doc.chapters.all()))
    data['title'] = doc.title
    data['version_date'] = doc.version_date
    data['last_update'] = doc.last_update
    data['tags'] = doc.tags_list()
    data['id'] = doc.id
    return JsonResponse(data)
