from django.contrib import admin

from .models import Chapter, Tag, Doc


class ChapterInline(admin.StackedInline):
    model = Chapter
    extra = 1
    min_num = 0


class DocAdmin(admin.ModelAdmin):
    inlines = (ChapterInline, )

admin.site.register(Tag)
admin.site.register(Doc, DocAdmin)
