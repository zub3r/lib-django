# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-01-29 12:36
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Chapter',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('order', models.PositiveSmallIntegerField(verbose_name='Order')),
                ('weight', models.PositiveSmallIntegerField(verbose_name='Weight')),
                ('title', models.CharField(max_length=511)),
                ('text', models.TextField(blank=True, verbose_name='Text')),
            ],
        ),
        migrations.CreateModel(
            name='Doc',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=511, verbose_name='Document Title')),
                ('version_date', models.DateField()),
                ('last_update', models.DateTimeField()),
            ],
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('slug', models.SlugField(max_length=20, verbose_name='Tag Code')),
                ('picture', models.ImageField(upload_to=b'images/tag_pictures/', verbose_name='Image')),
            ],
        ),
        migrations.AddField(
            model_name='doc',
            name='tags',
            field=models.ManyToManyField(to='library.Tag'),
        ),
        migrations.AddField(
            model_name='chapter',
            name='doc',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='chapters', to='library.Doc'),
        ),
        migrations.AlterUniqueTogether(
            name='chapter',
            unique_together=set([('doc', 'order')]),
        ),
    ]
