from django.conf.urls import url
from django.conf.urls.static import static

from docserver.library import views
from docserver import settings

urlpatterns = [
    url(r'^tags/?$', views.tags_list),
    url(r'^tags/(?P<tag_slug>\w+)/', views.docs_list),
    url(r'^docs/(?P<doc_pk>\d+)/', views.doc_detail),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
